<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Add Colleague                          _43f29c</name>
   <tag></tag>
   <elementGuidId>300f99f1-789a-4963-a8f8-c34406ee16ea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Global Vox'])[1]/following::div[15]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jconfirm-box jconfirm-hilight-shake jconfirm-type-default jconfirm-type-animated</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>jconfirm-box85</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>×Add Colleague

    

        
    











        
            
            
            
                
                    
                        
                            Las Vegas
                        
                        
                        
                        
                        
                            
                                
                                Washington
                                Las Vegas
                                
                            
                        

                    
                    
                        
                        
                    
                

            
            
                
                    

                        
                        
                        


                    
                    

                        
                        
                        

                    

                
            
            
                
                    

                        
                        
                        

                    
                    


                
            
            
                
                    

                        
                        
                        

                    

                    

                        

                        
                        
                        

                    
                    

                        
                        

                        

                        

                    
                

            
            
                
                    
                        
                            
                                

                                    Select role*
                                    

                                        Admin

                                        User

                                        Receptionist

                                        Host
                                        
                                    Host Select AllDeselect AllAdminUserReceptionistHost
                                    

                                
                            

                            
                        
                    
                
            
            
                


















                
            
            
                
                    
                        
                            
                            
                            Enable for login 
                        

                        
                    
                
            
        
        
        
        
        
            
        
        
            
        
    


Submitclose</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;jconfirm jconfirm-light jconfirm-open&quot;]/div[@class=&quot;jconfirm-scrollpane&quot;]/div[@class=&quot;jconfirm-row&quot;]/div[@class=&quot;jconfirm-cell&quot;]/div[@class=&quot;jconfirm-holder&quot;]/div[@class=&quot;jc-bs3-container container&quot;]/div[@class=&quot;jc-bs3-row row justify-content-md-center justify-content-sm-center justify-content-xs-center justify-content-lg-center&quot;]/div[@class=&quot;jconfirm-box-container jconfirm-animated col-md-6 col-md-offset-3 jconfirm-no-transition&quot;]/div[@class=&quot;jconfirm-box jconfirm-hilight-shake jconfirm-type-default jconfirm-type-animated&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Global Vox'])[1]/following::div[15]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[2]/div/div/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
