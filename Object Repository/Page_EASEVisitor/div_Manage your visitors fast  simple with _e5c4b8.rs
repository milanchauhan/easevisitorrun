<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Manage your visitors fast  simple with _e5c4b8</name>
   <tag></tag>
   <elementGuidId>e56f75a1-d5c8-4d67-a88c-c80860d9dc16</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page_content']/div/div/div/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Manage your visitors fast &amp; simple with EASEVisitor!
                        
                        
                            
    











                            
                                


                                
                                

                                    Admin
                                    Other
                                

                                
                                    

                                    
                                    Username

                                    
                                    


                                
                                
                                    Password
                                    
                                    
                                    
                                
                                
                                    
                                    
                                
        
        
        
        
        
        

                                
                                    
                                        Forgot
                                            Password?
                                    
                                    
                                        LOGIN
                                    
                                
                                
                                    
                                        Register Now
                                    
                                    
                                        Resend verification link
                                    
                                    

                                        
                                        Send
                                        


                                    

                                
                            
                        
                        
                            
                                
                                    
                                         Back
                                    
                                
                            
                            
                                Email
                                
                                
                                    
                                        SEND PASSWORD
                                            RESET LINK
                                        
                                    
                                
                            
                        
                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page_content&quot;)/div[@class=&quot;login&quot;]/div[@class=&quot;container full-height&quot;]/div[@class=&quot;row d-flex middle full-height&quot;]/div[@class=&quot;col-md-5 offset-md-1&quot;]/div[@class=&quot;card&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page_content']/div/div/div/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='visitor management solution'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
