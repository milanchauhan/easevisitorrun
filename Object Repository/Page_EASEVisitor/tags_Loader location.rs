<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tags_Loader location</name>
   <tag></tag>
   <elementGuidId>e7c8f278-632b-40f6-8a43-c9412ac237cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//i[@class='fas fa-spinner fa-spin']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>tags</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tagify form-control locationtagsC</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Washington
                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;jconfirm-box31953&quot;)/div[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/form[1]/div[@class=&quot;form-group&quot;]/div[@class=&quot;form-row&quot;]/div[@class=&quot;col-md-11&quot;]/tags[@class=&quot;tagify form-control locationtagsC&quot;]</value>
   </webElementProperties>
</WebElementEntity>
