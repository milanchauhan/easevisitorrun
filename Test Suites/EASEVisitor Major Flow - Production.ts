<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>EASEVisitor Major Flow - Production</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f6584d50-d617-4e21-a31e-8e9bde3440e2</testSuiteGuid>
   <testCaseLink>
      <guid>1df135bb-9400-42a3-ab34-fab688e9083e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/1. Verify Registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8c97d00-9288-4e17-80e0-b76e3b7ee77d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/2. Verify Registration Email</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6943d98e-c6bb-4ecb-a5eb-f7756bd9436c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7aee19cf-05b5-4f5f-a71a-71a0597afaa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3A. Verify Login</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d7c126f2-cb14-4e02-86cf-02eda7f76eec</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1c96eb53-8d78-480c-9c37-13b088676e4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/4. Verify default Document Added</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>46495217-fc63-46f1-a503-59d47b5bbcca</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c2faccaf-ac53-49f0-be87-692ed65fc81f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5. Verify Update default document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b9d1106-960e-4264-ad90-351c7b2a66c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/6. Verify Add new document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39e63670-aeca-4e68-8f1b-f10a181d9004</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7. Verify default Location is added</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c5756fa-1782-4c12-8c8c-1a0d328e8aff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/8. Verify Update default Location</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>295798a8-0681-4c01-9582-014129a2f775</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/9. Verify Add Location</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7e90b24-4976-418d-bb14-bded025af790</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10. Verify Admin added as default Colleague</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4aeed3f-2c38-444e-8a23-89c914e8e68c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11. Verify Add Colleague</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f0d015d-7206-429a-abad-0eac36479dc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12. Verify Edit Colleague</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18213c25-3ce0-4c04-98e5-a95eb13cb410</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/13AA. Verify Delete Visitor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62286f1b-ee01-4453-bfe2-10628f70837c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14. Verify Edit Visitor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3257766-1ee8-4696-9d98-1bcf0012cc9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/15. Verify Add Visit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd6469c5-2806-41f7-bdd9-48b2bd03c93f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/16. Verify Add Visit by Adding New Visitor, Colleague and Location</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3a3f8a4d-6f28-4ec7-ae56-518303314692</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>56c758e5-3f2b-4d32-8cb9-675d4795591d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/17. Verify Edit Visit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9220060a-ff46-4fe2-8eb7-ab8b10957fe3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/18. Verify Add Group Visit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b071075a-7419-4f35-9698-fd66712f4b5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/19. Verify Add Visit from Dashboard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f09c122-2d11-47d2-b141-1877579e3e06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/20. Verify Check-In Visitor</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>897552b6-3b40-433c-b244-90f83ad0d2a0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>02a008c9-35d1-41e6-92d1-d7c2c33af542</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/21. Verify Check-Out Visitor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39ad2830-0e47-4e2b-807c-fb8ecbe594f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/22. Verify No Show Visitor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>543e9661-659e-4e92-96b5-74664d0e7444</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/23. Verify Delete Visit</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
