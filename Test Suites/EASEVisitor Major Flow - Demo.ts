<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>EASEVisitor Major Flow - Demo</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>caa59e17-d15f-4932-a0fe-9d1864eece6c</testSuiteGuid>
   <testCaseLink>
      <guid>47b49084-b457-4f0c-a281-6f6a515f543f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1. Verify Registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a7e936f7-eef8-4ee6-9824-50c451f9cb9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2. Verify Registration Email</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6943d98e-c6bb-4ecb-a5eb-f7756bd9436c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7aee19cf-05b5-4f5f-a71a-71a0597afaa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3. Verify Login</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3a3f8a4d-6f28-4ec7-ae56-518303314692</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>17746eb2-6dc9-421c-bb9c-78cc297b71e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/4. Verify default Document Added</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>46495217-fc63-46f1-a503-59d47b5bbcca</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8ae2adff-6bc4-4d29-b4a9-f5cf4d651ee7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5. Verify Update default document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b9d1106-960e-4264-ad90-351c7b2a66c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/6. Verify Add new document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0363c0e-eb6f-42eb-8616-1c28cfe3df5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/7. Verify default Location is added</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4299f174-7fcd-405c-999d-c95a673cac05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/8. Verify Update default Location</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>295798a8-0681-4c01-9582-014129a2f775</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/9. Verify Add Location</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81cd0715-6968-4e73-b4ac-1146c2fbc5b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10. Verify Admin added as default Colleague</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4aeed3f-2c38-444e-8a23-89c914e8e68c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11. Verify Add Colleague</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f0d015d-7206-429a-abad-0eac36479dc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12. Verify Edit Colleague</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18213c25-3ce0-4c04-98e5-a95eb13cb410</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/13AA. Verify Delete Visitor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62286f1b-ee01-4453-bfe2-10628f70837c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/14. Verify Edit Visitor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3257766-1ee8-4696-9d98-1bcf0012cc9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/15. Verify Add Visit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd6469c5-2806-41f7-bdd9-48b2bd03c93f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/16. Verify Add Visit by Adding New Visitor, Colleague and Location</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3a3f8a4d-6f28-4ec7-ae56-518303314692</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>56c758e5-3f2b-4d32-8cb9-675d4795591d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/17. Verify Edit Visit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9220060a-ff46-4fe2-8eb7-ab8b10957fe3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/18. Verify Add Group Visit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b071075a-7419-4f35-9698-fd66712f4b5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/19. Verify Add Visit from Dashboard</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
