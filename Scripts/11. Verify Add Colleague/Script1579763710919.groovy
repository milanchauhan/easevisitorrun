import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Colleagues'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Add                        Colleague'))

WebUI.click(findTestObject('Page_EASEVisitor/tags_Add Colleague_tagify form-control loca_1be198'))

WebUI.delay(2)

WebUI.setText(findTestObject('Page_EASEVisitor/tags_Add Colleague_tagify form-control loca_1be198'), GlobalVariable.randomStr + 
    findTestData('EASEVisitor Test Data').getValue(2, 6))

WebUI.delay(2)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Add Colleague_tagify form-control loca_1be198'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_firstName'), GlobalVariable.randomStr + 
    findTestData('EASEVisitor Test Data').getValue(2, 8))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_lastName'), findTestData('EASEVisitor Test Data').getValue(
        2, 9))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_email'), findTestData('EASEVisitor Test Data').getValue(
        2, 10))

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_countryCode'), '1')

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_mobile'), '9000000000')

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/div_Host'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Host'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Host'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_EASEVisitor/select_Admin                               _72a283'), 
    'QORjgxfHcSw=', true)

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/div_Add Colleague                          _43f29c'))

WebUI.click(findTestObject('Page_EASEVisitor/label_Enable for login'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/label_Enable for login'))

WebUI.click(findTestObject('Page_EASEVisitor/button_Submit'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_EASEVisitor/button_ok'))

WebUI.delay(2)

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 6), false)

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 8), false)

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 9), false)

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 10), false)

