import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(findTestData('EASEVisitor Test Data').getValue(4, 1))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/b_Register Now'))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_First_firstName'), GlobalVariable.randomStr+findTestData('EASEVisitor Test Data').getValue(
        2, 1))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Last_lastName'), findTestData('EASEVisitor Test Data').getValue(
        2, 2))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Company name_companyName'), GlobalVariable.randomStr+findTestData('EASEVisitor Test Data').getValue(
        2, 3))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Phone number_phone'), '9000000000')

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Email_email'), GlobalVariable.randomStr+findTestData('EASEVisitor Test Data').getValue(
        2, 4))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Country_postalCode'), '88901')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_EASEVisitor/input_Password_password'), '7Dxoto1EjKfQbGop/Ov+1g==')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_EASEVisitor/input_Confirm password_confirmPassword'), '7Dxoto1EjKfQbGop/Ov+1g==')

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Register'))

not_run: WebUI.closeBrowser()

