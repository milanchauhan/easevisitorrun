import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.openBrowser('')

not_run: WebUI.maximizeWindow()

not_run: WebUI.navigateToUrl(findTestData('EASEVisitor Test Data').getValue(4, 1))

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Username_username'), findTestData('EASEVisitor Test Data').getValue(
        2, 4))

not_run: WebUI.setEncryptedText(findTestObject('Page_EASEVisitor/input_Password_password'), '7Dxoto1EjKfQbGop/Ov+1g==')

not_run: WebUI.sendKeys(findTestObject('Page_EASEVisitor/input_Password_password'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/span_Documents'))

WebUI.verifyTextPresent('Terms', false)

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_document_defaultpdf'))

WebUI.closeWindowIndex(1)

WebUI.delay(1)

WebUI.switchToWindowIndex(0)

WebUI.delay(1)

WebUI.verifyTextPresent('Terms', false)

not_run: WebUI.click(findTestObject('Page_EASEVisitor/i_Locations_fas fa-user-circle fa-2x t-gray'))

not_run: WebUI.click(findTestObject('Page_EASEVisitor/a_Logout'))

not_run: WebUI.closeBrowser()

