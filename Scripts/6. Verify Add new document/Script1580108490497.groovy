import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.openBrowser('')

not_run: WebUI.maximizeWindow()

not_run: WebUI.navigateToUrl('http://150.136.137.135:8080/login')

not_run: WebUI.setText(findTestObject('Page_EASEVisitor/input_Username_username'), findTestData('EASEVisitor Test Data').getValue(
        2, 4))

not_run: WebUI.setEncryptedText(findTestObject('Page_EASEVisitor/input_Password_password'), '7Dxoto1EjKfQbGop/Ov+1g==')

not_run: WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/div_Manage your visitors fast  simple with _e5c4b8'))

not_run: WebUI.click(findTestObject('Page_EASEVisitor/button_LOGIN'))

WebUI.click(findTestObject('Page_EASEVisitor/span_Documents'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Add New Document'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Add Document_documentName'), GlobalVariable.randomStr+findTestData('EASEVisitor Test Data').getValue(
        2, 33))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Add Document_documentDescription'), GlobalVariable.randomStr+findTestData(
        'EASEVisitor Test Data').getValue(2, 33))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Attach PDF'))

WebUI.delay(2)

dd()

not_run: WebUI.uploadFile(findTestObject('Page_EASEVisitor/input_Document_pdf'), 'C:\\Users\\91886\\Downloads\\pdf.pdf')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Submit'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_ok'))

WebUI.verifyTextPresent(GlobalVariable.randomStr+findTestData('EASEVisitor Test Data').getValue(2, 33), false)

not_run: WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Delivery Flowpdf'))

not_run: WebUI.closeBrowser()

def dd() {
    Runtime runTime = Runtime.getRuntime()

    Process process = runTime.exec('C:\\Katalon Projects\\EASEVisitor\\AutoIT Scripts\\DocumentAdd.exe')
}

