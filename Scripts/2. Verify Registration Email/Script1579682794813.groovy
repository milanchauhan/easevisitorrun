import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://www.yopmail.com/en/')

WebUI.setText(findTestObject('Object Repository/Page_YOPmail - Disposable Email Address/input_Type the Email name of your choice_login'), 
    Email)

WebUI.click(findTestObject('Object Repository/Page_YOPmail - Disposable Email Address/input_Type the Email name of your choice_sbut'))

WebUI.click(findTestObject('Object Repository/Page_YOPmail - Inbox/span_SPAMeaseocrocr-inccom'))

WebUI.switchToWindowTitle('YOPmail - Inbox')

WebUI.click(findTestObject('Object Repository/Page_YOPmail - Inbox/a_Verify            Email Address'))

WebUI.delay(5)

not_run: WebUI.navigateToUrl('http://150.136.137.135:8080/login')

not_run: WebUI.setText(findTestObject('Page_EASEVisitor/input_Loginemail_loginemail'), Email)

not_run: WebUI.setEncryptedText(findTestObject('Page_EASEVisitor/input_Password_password'), '7Dxoto1EjKfQbGop/Ov+1g==')

not_run: WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_LOGIN'))

not_run: WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Disable'))

not_run: WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/span_Dashboard'))

WebUI.closeBrowser()

