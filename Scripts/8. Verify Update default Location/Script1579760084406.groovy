import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Page_EASEVisitor/a_Locations'))

WebUI.click(findTestObject('Page_EASEVisitor/button_Edit'))

WebUI.setText(findTestObject('Page_EASEVisitor/input_concat(Edit The Lakes  s Details)_loc_cdcfde'), GlobalVariable.randomStr+findTestData('EASEVisitor Test Data').getValue(
        2, 6))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_concat(Edit The Lakes  s Details)_postalCode'), '20001')

WebUI.sendKeys(findTestObject('Page_EASEVisitor/input_concat(Edit The Lakes  s Details)_postalCode'), Keys.chord(Keys.TAB))

WebUI.waitForElementNotVisible(findTestObject('Page_EASEVisitor/tags_Loader location'), 10)

WebUI.delay(2)

WebUI.click(findTestObject('Page_EASEVisitor/button_Submit'))

WebUI.click(findTestObject('Page_EASEVisitor/button_ok'))

WebUI.verifyTextPresent(GlobalVariable.randomStr+findTestData('EASEVisitor Test Data').getValue(2, 6), false)

not_run: WebUI.delay(3)

