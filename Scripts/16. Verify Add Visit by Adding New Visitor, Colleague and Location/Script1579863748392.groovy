import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/span_Logbook'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Add New Visit'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits_visitor'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/button_Plus_visitor'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_firstName'), GlobalVariable.randomStr + 
    findTestData('EASEVisitor Test Data').getValue(2, 23))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_lastName'), findTestData(
        'EASEVisitor Test Data').getValue(2, 24))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_email'), findTestData(
        'EASEVisitor Test Data').getValue(2, 25))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_cmpny'), GlobalVariable.randomStr + 
    findTestData('EASEVisitor Test Data').getValue(2, 26))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_address1'), '111, Street')

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_address2'), 'Avenue')

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_postalCode'), '88901')

WebUI.sendKeys(findTestObject('Page_EASEVisitor/input_Click here to invite visitor_postalCode'), Keys.chord(Keys.TAB))

WebUI.waitForElementNotVisible(findTestObject('Page_EASEVisitor/tags_Loader location'), 10)

WebUI.click(findTestObject('Page_EASEVisitor/input_Visitor_phone'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.setText(findTestObject('Page_EASEVisitor/tags_Plus_Visits_country'), 'United States')

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Plus_Visits_country active'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/input_Visitor_phone'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Page_EASEVisitor/input_Visitor_phone'), '900000000')

WebUI.click(findTestObject('Page_EASEVisitor/button_Submit'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/button_ok'))

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/button_Plus_colleague'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_firstName'), GlobalVariable.randomStr + 
    findTestData('EASEVisitor Test Data').getValue(2, 27))

WebUI.setText(findTestObject('Page_EASEVisitor/tags_Plus_Visits_locaction'), GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(
        2, 6))

WebUI.delay(2)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Plus_Visits_location active'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_lastName'), findTestData('EASEVisitor Test Data').getValue(
        2, 28))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_email'), findTestData('EASEVisitor Test Data').getValue(
        2, 29))

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_countryCode'), '1')

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_mobile'), '9000000000')

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/div_Host'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Host'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Host'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_EASEVisitor/select_Admin                               _72a283'), 
    'QORjgxfHcSw=', true)

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/div_Add Colleague                          _43f29c'))

WebUI.click(findTestObject('Page_EASEVisitor/label_Enable for login'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/label_Enable for login'))

WebUI.click(findTestObject('Page_EASEVisitor/button_Submit'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/button_Plus_location'))

WebUI.delay(1)

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Add location_locationName'), GlobalVariable.randomStr + 
    findTestData('EASEVisitor Test Data').getValue(2, 30))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Add location_address1'), '111, Street')

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Add location_postalCode'), '10001')

WebUI.sendKeys(findTestObject('Page_EASEVisitor/input_Add location_postalCode'), Keys.chord(Keys.TAB))

WebUI.waitForElementNotVisible(findTestObject('Page_EASEVisitor/tags_Loader location'), 10)

WebUI.setText(findTestObject('Page_EASEVisitor/input_Visitor_phone'), '9000000000')

WebUI.click(findTestObject('Page_EASEVisitor/button_Submit'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits'))

WebUI.delay(1)

not_run: WebUI.setText(findTestObject('Page_EASEVisitor/tags_Visits_meetingfrom'), findTestData('EASEVisitor Test Data').getValue(
        2, 31))

WebUI.delay(1)

WebUI.selectOptionByValue(findTestObject('Page_EASEVisitor/select_From_Hours'), findTestData('EASEVisitor Test Data').getValue(
        2, 37), true)

WebUI.selectOptionByValue(findTestObject('Page_EASEVisitor/select_From_Mins'), findTestData('EASEVisitor Test Data').getValue(
        2, 38), true)

not_run: WebUI.setText(findTestObject('Page_EASEVisitor/tags_Visits_meetingto'), findTestData('EASEVisitor Test Data').getValue(
        2, 32))

WebUI.selectOptionByValue(findTestObject('Page_EASEVisitor/select_To_Hours'), findTestData('EASEVisitor Test Data').getValue(
        2, 39), true)

WebUI.selectOptionByValue(findTestObject('Page_EASEVisitor/select_To_Mins'), findTestData('EASEVisitor Test Data').getValue(
        2, 40), true)

WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits'))

WebUI.click(findTestObject('Page_EASEVisitor/button_Submit'))

WebUI.click(findTestObject('Page_EASEVisitor/button_ok'))

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 23), false)

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 24), false)

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 26), false)

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 27), false)

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 28), false)

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 30), false)

WebUI.delay(3)

not_run: WebUI.click(findTestObject('Page_EASEVisitor/i_Locations_fas fa-user-circle fa-2x t-gray'))

not_run: WebUI.click(findTestObject('Page_EASEVisitor/a_Logout'))

not_run: WebUI.closeBrowser()

