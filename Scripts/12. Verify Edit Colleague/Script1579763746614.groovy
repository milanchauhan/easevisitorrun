import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Page_EASEVisitor/span_Colleagues'))

WebUI.click(findTestObject('Page_EASEVisitor/button_Edit'))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_firstName'), GlobalVariable.randomStr + 
    findTestData('EASEVisitor Test Data').getValue(2, 11))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_lastName'), findTestData('EASEVisitor Test Data').getValue(
        2, 12))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Las Vegas_email'), findTestData('EASEVisitor Test Data').getValue(
        2, 13))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/div_Host'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_User'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_EASEVisitor/select_Admin                               _72a283'), 
    'QORjgxfHcSw=', true)

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/div_Add Colleague                          _43f29c'))

WebUI.click(findTestObject('Page_EASEVisitor/button_Submit'))

WebUI.click(findTestObject('Page_EASEVisitor/button_ok'))

not_run: WebUI.click(findTestObject('Page_EASEVisitor/i_Locations_fas fa-user-circle fa-2x t-gray'))

not_run: WebUI.click(findTestObject('Page_EASEVisitor/a_Logout'))

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 11), false)

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 12), false)

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 13), false)

not_run: WebUI.closeBrowser()

