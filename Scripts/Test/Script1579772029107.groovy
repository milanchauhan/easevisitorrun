import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

WebUI.openBrowser('')

WebUI.navigateToUrl('http://150.136.137.135:8080/login')

WebUI.setText(findTestObject('Page_EASEVisitor/input_Username_username'), GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(
        2, 4))

WebUI.setEncryptedText(findTestObject('Page_EASEVisitor/input_Password_password'), '7Dxoto1EjKfQbGop/Ov+1g==')

WebUI.sendKeys(findTestObject('Page_EASEVisitor/input_Password_password'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/span_Logbook'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/input_Share Link_visitor'))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Share Link_visitor'), GlobalVariable.randomStr + 
    findTestData('EASEVisitor Test Data').getValue(2, 43))

WebUI.sendKeys(findTestObject('Object Repository/Page_EASEVisitor/input_Share Link_visitor'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Toggle Dropdown'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Change to            checked in'))

WebUI.click(findTestObject('Page_EASEVisitor/button_ok'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/span_Checked In'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/span_Address Book'))

WebUI.setText(findTestObject('Page_EASEVisitor/input_-_first_name'), 'JJWJohn')

WebUI.sendKeys(findTestObject('Page_EASEVisitor/input_-_first_name'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 43), false)

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 44), false)

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 11), false)

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 12), false)

WebUI.closeBrowser()

