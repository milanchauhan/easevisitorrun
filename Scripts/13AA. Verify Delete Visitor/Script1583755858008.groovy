import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://150.136.137.135:8080/login')

WebUI.setText(findTestObject('Page_EASEVisitor/input_Username_username'), GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(
        2, 4))

WebUI.setEncryptedText(findTestObject('Page_EASEVisitor/input_Password_password'), '7Dxoto1EjKfQbGop/Ov+1g==')

WebUI.click(findTestObject('Page_EASEVisitor/button_LOGIN'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/span_Address Book'))

not_run: WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Add Visitor'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_firstName'), 
    (GlobalVariable.randomStr + 'D') + findTestData('EASEVisitor Test Data').getValue(2, 14))

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_lastName'), 
    'D' + findTestData('EASEVisitor Test Data').getValue(2, 15))

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_email'), 'D' + 
    findTestData('EASEVisitor Test Data').getValue(2, 16))

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_cmpny'), (GlobalVariable.randomStr + 
    'D') + findTestData('EASEVisitor Test Data').getValue(2, 17))

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_address1'), 
    '111, Street')

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_address2'), 
    'Avenue')

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_postalCode'), 
    '88901')

not_run: WebUI.sendKeys(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_postalCode'), 
    Keys.chord(Keys.TAB))

not_run: WebUI.waitForElementNotVisible(findTestObject('Object Repository/Page_EASEVisitor/tags_Loader location'), 10)

not_run: WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/tags_Add Colleague_tagify form-control loca_1be198'))

not_run: WebUI.delay(2)

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/tags_Add Colleague_tagify form-control loca_1be198'), 
    'United States')

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/tags_Add Colleague_tagify form-control loca_1be198'))

not_run: WebUI.delay(1)

not_run: WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Visitor_phone'), '900000000')

not_run: WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Submit'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_ok'))

not_run: WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/span_Address Book'))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_-_first_name'), 'JJWDCarry')

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_JJWDCarry DScoth'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Delete Profile'))

WebUI.click(findTestObject('Page_EASEVisitor/button_delete1'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_ok'))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_-_first_name'), 'JJWDCarry')

WebUI.verifyTextNotPresent((GlobalVariable.randomStr + 'D') + findTestData('EASEVisitor Test Data').getValue(2, 14), false)

WebUI.verifyTextNotPresent('D' + findTestData('EASEVisitor Test Data').getValue(2, 15), false)

WebUI.delay(1)

