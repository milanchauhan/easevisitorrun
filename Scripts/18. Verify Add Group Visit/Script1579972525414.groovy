import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/span_Logbook'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Add New Visit'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits_visitor'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_EASEVisitor/tags_Visits_visitor'), GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(
        2, 21))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits_visitor'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_EASEVisitor/tags_Visits_visitor'), GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(
        4, 25))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_EASEVisitor/input_Group'), 'CIA')

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits_colleague'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_EASEVisitor/tags_Visits_colleague'), GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(
        2, 22))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits_location'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_EASEVisitor/tags_Visits_location'), GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(
        2, 6))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits'))

WebUI.delay(1)

not_run: WebUI.setText(findTestObject('Page_EASEVisitor/tags_Visits_meetingfrom'), findTestData('EASEVisitor Test Data').getValue(
        2, 31))

WebUI.delay(1)

WebUI.selectOptionByValue(findTestObject('Page_EASEVisitor/select_From_Hours'), findTestData('EASEVisitor Test Data').getValue(
        2, 37), true)

WebUI.selectOptionByValue(findTestObject('Page_EASEVisitor/select_From_Mins'), findTestData('EASEVisitor Test Data').getValue(
        2, 38), true)

not_run: WebUI.setText(findTestObject('Page_EASEVisitor/tags_Visits_meetingto'), findTestData('EASEVisitor Test Data').getValue(
        2, 32))

WebUI.selectOptionByValue(findTestObject('Page_EASEVisitor/select_To_Hours'), findTestData('EASEVisitor Test Data').getValue(
        2, 39), true)

WebUI.selectOptionByValue(findTestObject('Page_EASEVisitor/select_To_Mins'), findTestData('EASEVisitor Test Data').getValue(
        2, 40), true)

WebUI.delay(1)

not_run: WebUI.click(findTestObject('Page_EASEVisitor/tags_Visits'))

WebUI.click(findTestObject('Page_EASEVisitor/button_Submit'))

WebUI.click(findTestObject('Page_EASEVisitor/button_ok'))

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 6), false)

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 18), false)

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 19), false)

WebUI.verifyTextPresent(GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(2, 20), false)

WebUI.delay(3)

