import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/span_Address Book'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Add Visitor'))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_firstName'), GlobalVariable.randomStr+findTestData(
        'EASEVisitor Test Data').getValue(2, 14))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_lastName'), findTestData(
        'EASEVisitor Test Data').getValue(2, 15))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_email'), findTestData(
        'EASEVisitor Test Data').getValue(2, 16))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_cmpny'), GlobalVariable.randomStr+findTestData(
        'EASEVisitor Test Data').getValue(2, 17))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_address1'), '111, Street')

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_address2'), 'Avenue')

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Click here to invite visitor_postalCode'), '88901')

WebUI.sendKeys(findTestObject('Page_EASEVisitor/input_Click here to invite visitor_postalCode'), Keys.chord(Keys.TAB))

WebUI.waitForElementNotVisible(findTestObject('Page_EASEVisitor/tags_Loader location'), 10)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Add Colleague_tagify form-control loca_1be198'))

WebUI.delay(2)

WebUI.setText(findTestObject('Page_EASEVisitor/tags_Add Colleague_tagify form-control loca_1be198'), 'United States')

WebUI.delay(2)

WebUI.click(findTestObject('Page_EASEVisitor/tags_Add Colleague_tagify form-control loca_1be198'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_EASEVisitor/input_Visitor_phone'), '900000000')

WebUI.click(findTestObject('Page_EASEVisitor/button_Submit'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_EASEVisitor/button_ok'))

WebUI.delay(1)

WebUI.verifyTextPresent(GlobalVariable.randomStr+findTestData('EASEVisitor Test Data').getValue(2, 14), false)

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 15), false)

