import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(findTestData('EASEVisitor Test Data').getValue(4, 1))

WebUI.setText(findTestObject('Page_EASEVisitor/input_Username_username'), GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(
        2, 4))

WebUI.setEncryptedText(findTestObject('Page_EASEVisitor/input_Password_password'), '7Dxoto1EjKfQbGop/Ov+1g==')

WebUI.click(findTestObject('Page_EASEVisitor/button_LOGIN'))

WebUI.click(findTestObject('Page_EASEVisitor/i_Locations_fas fa-user-circle fa-2x t-gray'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Invite Visitor'))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Invite Visitor_firstName'), GlobalVariable.randomStr +findTestData('EASEVisitor Test Data').getValue(
        2, 43))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Invite Visitor_lastName'), findTestData('EASEVisitor Test Data').getValue(
        2, 44))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Invite Visitor_email'), findTestData('EASEVisitor Test Data').getValue(2, 45))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Invite Visitor'))

WebUI.delay(2)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://www.yopmail.com/en/')

WebUI.setText(findTestObject('Page_YOPmail - Disposable Email Address/input_Type the Email name of your choice_login'), findTestData('EASEVisitor Test Data').getValue(2, 45))

WebUI.click(findTestObject('Page_YOPmail - Disposable Email Address/input_Type the Email name of your choice_sbut'))

WebUI.click(findTestObject('Page_YOPmail - Inbox/a_Verify_click here'))

WebUI.switchToWindowIndex(1)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Visitor form_address1'), '11, Street')

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Visitor form_cmpny'), 'JAJ')

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Visitor form_postalCode'), '89001')

WebUI.sendKeys(findTestObject('Page_EASEVisitor/input_Click here to invite visitor_postalCode'), Keys.chord(Keys.TAB))

WebUI.waitForElementNotVisible(findTestObject('Page_EASEVisitor/tags_Loader location'), 10)

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_United States_specialityNotes'), 'VIP')

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Save'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/div_You are added as a visitor successfully'))

WebUI.closeBrowser()

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(findTestData('EASEVisitor Test Data').getValue(4, 1))

WebUI.setText(findTestObject('Page_EASEVisitor/input_Username_username'), GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(
        2, 4))

WebUI.setEncryptedText(findTestObject('Page_EASEVisitor/input_Password_password'), '7Dxoto1EjKfQbGop/Ov+1g==')

WebUI.click(findTestObject('Page_EASEVisitor/button_LOGIN'))

WebUI.click(findTestObject('Page_EASEVisitor/i_Locations_fas fa-user-circle fa-2x t-gray'))

WebUI.click(findTestObject('Page_EASEVisitor/span_Address Book'))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_-_first_name'), GlobalVariable.randomStr + findTestData(
        'EASEVisitor Test Data').getValue(2, 43))

WebUI.sendKeys(findTestObject('Page_EASEVisitor/input_-_first_name'), Keys.chord(Keys.ENTER))

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 43), false)

WebUI.verifyTextPresent(findTestData('EASEVisitor Test Data').getValue(2, 44), false)

WebUI.closeBrowser()

