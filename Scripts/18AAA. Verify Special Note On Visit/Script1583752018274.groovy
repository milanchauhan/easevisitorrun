import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://150.136.137.135:8080/login')

WebUI.setText(findTestObject('Page_EASEVisitor/input_Username_username'), GlobalVariable.randomStr + findTestData('EASEVisitor Test Data').getValue(
        2, 4))

WebUI.setEncryptedText(findTestObject('Page_EASEVisitor/input_Password_password'), '7Dxoto1EjKfQbGop/Ov+1g==')

WebUI.click(findTestObject('Page_EASEVisitor/button_LOGIN'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/span_Logbook'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/input_Share Link_visitor'))

WebUI.setText(findTestObject('Object Repository/Page_EASEVisitor/input_Share Link_visitor'), GlobalVariable.randomStr + 
    findTestData('EASEVisitor Test Data').getValue(2, 43))

WebUI.sendKeys(findTestObject('Object Repository/Page_EASEVisitor/input_Share Link_visitor'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/button_Toggle Dropdown'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/a_Change to            checked in'))

WebUI.verifyTextPresent('VIP', false)

WebUI.click(findTestObject('Page_EASEVisitor/button_ok'))

WebUI.click(findTestObject('Object Repository/Page_EASEVisitor/span_Checked In'))

WebUI.delay(1)

WebUI.closeBrowser()

